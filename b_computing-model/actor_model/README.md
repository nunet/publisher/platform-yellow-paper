# Actor model

## Introduction

Actor model provides NuNet with ability to express and implement a potentially infinite space of concurrent, non-locking, scalable and fault-tolerant computations; This component is crucial for enabling non-trivial computing in a decentralized (without single point of control, coordination or ownership) network of heterogenous devices and processes communicating via unsafe and unstable network connections (i.e. Internet). 

## Definition and properties

The actor model is a mathematical model of concurrent computation, proposed by Hewitt, Bishop, and Steiger (1973) and further developed by Clinger (1981), Greif (1975), Agha (1986) and others. It is conceptually based on one kind of object – an autonomous communicating actor (processor, agent, etc.) which does not presuppose any representation of primitive data or control structures. Data structures can be programmed or hard-wired and encapsulated or dynamically evolved to each actor separately or to an ensemble. 

Actors communicate via immutable messages and in this sense constitute a message-passing framework. Patterns of message passing between actors is the only control structure which defines the functionality and behaviour of a particular actor system as a whole. There are no constraints on message-passing patterns or even requirements for their existence by the lowest level of the actor model. System engineers enforce these constraints via high level design or let them emerge during computation.

An actor itself is a self-contained, interacting, independent component having a well defined behaviour and a mail address. An actor’s behaviour is expressed by a computational process having inputs, outputs and computational resource requirements, including storage. Actors communicate with each other via asynchronous message passing using their mail addresses. 

In summary, an **Actor** is the universal (and the only) primitive of concurrent computation, that can:
   1. receive a message from other actors;
   2. make local decisions / perform computations [possibly in response to messages];
   3. send messages to other actors;
   4. create other actors;
   5. maintain local state (and therefore be able to respond to messages differently);

<img src="https://gitlab.com/nunet/publisher/platform-yellow-paper/-/raw/main/b_computing-model/actor_model/assets/actor.png" alt="A graphical illustration of an actor" width=50%>

All computations in the actor model are built by these primitives only, which are necessarily executed on behalf of an actor in an actor system. No primitive can be executed without an actor that executes it.



## Requirements for implementation

The main requirements of the model are (rigorously followed in all levels of implementation of NuNet platform):

### Decoupling senders from communication

The fundamental advance of the actor model was the decoupling of a sender from the communications it sends – once a message has been sent, it loses any relation to the sender and is delivered on a best effort basis. This is a sharp difference from other models of concurrent computation, where message sending is tightly coupled to the sender, which synchronously transfers the message to some persistent structure – e.g. buffer, queue, mailbox, channel, broker or server – for temporary storage and retrieval by a reader. 

This requirement results is the absence of the global state in the actor model and allows it to embrace inconsistencies inherent in large decentralized systems. Actually, consistency and synchronous execution are special cases within the actor model and have to be explicitly enforced if considered necessary by designers.

In order to prove mathematical properties of actor model, message sending is even not considered an event in the formal representation of the model. 

### Private state

Each actor implement a conventional sequential or parallel computing. Actually, actor model does not constrain what and how actors do computation inside their boundaries. Actors can modify their own private state (or decide to expose to other actors via message communication), but can affect each other only indirectly through messages asking for certain behaviors via remote-procedure-call-type requests;

<img src="assets/actor_model_and_system.png" alt="Actor model and system" width=100%>

### No a-priori assumptions about network

Actors do not pre-assume anything outside their boundaries, and therefore there is no a-priori trust between actors or, in this sense, in system as a whole; any trust between actors has to be negotiated and established peer-wise. This principle does not prevent from defining external trust providers, which can be used by actors in the network in case they explicitly 

### No assumption of global state / omniscient observer

4. There is no *global* knowledge or *global* controller that has access to the whole system and therefore any controlling mechanism should be implemented via an actor which needs to establish trust relations with other actors in the system;

## Object-capability model

Object-capability model [@RobustComposition] conceptually is very similar to actor model of computation but provides additional concepts for reasoning about security and permissions model in the decentralized system with by default 'zero-trust' and and 'end-point security', which are the basic principles of NuNet platform security architecture.

Capability-based security is a concept in the design of secure computing systems (not only decentralized!) where **capability** is a communicable, unforgeable token of authority, which can be attached to each message / request between peers. This token necessarily includes: 

1. unique **key** of the issuing actor (e.g. public key);
2. **audience** of the message (paths or addresses of the target actors(s));
3. **capability** declarations -- declaration of what resources can be accessed;

Importantly for a decentralized computing, object-capability model allows capabilities to be delegated -- i.e. an actor can propagate its capabilities to other actors. A token of authority where capability is delegated includes the proof of delegation chain -- without centralized Access Control List or other centralized authority. Capability-based security is often contrasted with traditional UNIX permissions and Access Control Lists. 

## High level system properties provided by Actor model

Implementation of actor model achieves the following higher level properties of the decentralized computing ecosystem. Some of these properties were listed as [computational and functional requirements](https://docs.nunet.io/nunet-whitepaper/system-architecture/computational-and-functional-principles) in NuNet's initial whitepaper 2.0.

### Mobility

A key issue in developing multi-agent systems that the actor model allows us to address at a fundamental level is _mobility_. The implementation of the model requires computing resources for enabling processing, storage and communication for processes which actors encapsulate, which is precisely the rationale behind NuNet framework.

The physical implementations of such computing resources are by convention called nodes – i.e. servers, processors, computing centres, etc. – that together make a distributed computing infrastructure of cloud infrastructures, sub-networks and eventually a global network. Mobility allows an agent to migrate freely from one such node in a distributed computing environment to another to seek "better" execution environments.

This flexibility can be approached from at least two aspects – a computer-scientific and economic – both stretching the limits of computational systems as usually understood, which we are attempting to do at NuNet.

### Customizable execution contexts

Customizable execution contexts and computational reflection enable individual actors to have a continuous interaction with their environment to determine available resources, relate to their own state, search for and request additional resources and by that provide evolving resource consumption strategies. These are needed for an agent to specify requirements for negotiation with a hosting node, besides the bare estimation of computational resource requirements – e.g. programming languages, library dependencies, hardware components, containerization engines, etc.

### Coordination and interaction

The power of the actor model comes from the exploitation of parallelism, distribution and mobility in ensembles of agents. It promises orders of magnitude greater computational abilities than that of individual actors, which are nothing more than conventional sequential programs. As each problem-solving agent possesses only an incomplete local view of the system and limited computational power, it must coordinate with other agents in order to achieve coherent and globally viable solutions. Since in the actor model there is no global coordinating actor, it has to happen from the bottom-up via synchronization, coherence and functional coordination. In order to provide a minimal basis for coordination in a computational medium such as an actor system, interaction protocols and policies have to be in place. Apart from functional coordination, interaction policies enable the system to deal with failures, which is the norm rather than the exception in distributed and decentralized systems.

## Related resources

* [NuNet Technical Discussion - Architecture - June 16th 2022](https://youtu.be/0_ViQUi390k?si=gA7nDcuznWZ-orYv)
* [NuNet Technical Discussion - Introduction to Actor Model - March 7, 2024](https://drive.google.com/file/d/1qTJku2TYuXpgrShSDS4eJuC-uM9BnTGA/view)
* [Kabir's doctoral thesis - Actor model and framework - March 21, 2019](https://kabirkbr.github.io/public/synthetic_cognitive_development/kabir_thesis_20190321_official_submission.pdf#section.5.1)
* [Implementation of Offer Networks concept SingularityNET -- computational framework documentation](https://singnet.github.io/offernet/public/offernet-documentation/computational-framework.html)
	
