# NuNet model of computing

## Actor model + graph computing

The concept of global economy of decentralized computing rests on surprisingly large body of research done since the dawn of modern computing era (e.g. @RobustComposition). The idea, models and formalisms of decentralized computing and ecosystems of autonomous communicating computing processes almost seem as an alternative path of the computing industry / universe which has never ceased to develop, but was somewhat overshadowed by the mainstream 'centralized' computing paradigms, models and philosophies. During at least half-decade of research and industry development it seems that most of the pieces of the puzzle are in place for this path to resurface.

NuNet is primarily based on the **Actor model of computation** [@hewitt_actor_model_2010]. The major premise and background of Actor model of computation (also shared by the object-capability model), is addressing the complexity of modern system designs not by seeking universal solutions to ever increasing coordination problems, but by providing maximum autonomy for smallest components of the system and providing abstraction mechanisms for crafting diverse solutions adapted to the needs of many applications. In the context of *economy of interacting computer processes (i.e. system components)* -- the context in which NuNet is operating -- this premise is a necessary condition.

Second major component of NuNet's conceptual design is the **Graph Computing**, which allows for interacting processes to access the topology of the network (into which they are embedded) via local neighbors connections. Graph Computing obviously has roots in graph theory, but extends it to practical contexts. We extend and modify it further for our purposes.  

The unification of **Actor model** and **Graph computing** frameworks produce the concept of of 'open-ended decentralized computing' which is the conceptual inspiration for NuNet's computing model. As important as this concept is for our NuNet's computing model, detailed description / discussion of it is outside the scope of this document. Please refer to prior research on this [topic](https://kabirkbr.github.io/public/synthetic_cognitive_development/kabir_thesis_20190321_official_submission.pdf#chapter.5) for more details. **Actor model** and **Graph computing** frameworks  contain fairly large bodies of computer-scientific knowledge as well as adaptations for NuNet. They are detailed in separate sections: 

* **B.1. Computing model / Actor model of computation**
* **B.2. Computing model / Graph computing**

## Implementation aspects 

Following the expression of the global network of computing, we can [approximately] map each identified aspect with its implementation within the NuNet computing model and its main software component - [device-management-service](https://docs.nunet.io/public-documentation/public-technical-documentation/device-management-service/develop).

$$
     NuNet = \\
       (executables \cup data \cup state) \cup (resources \cup connections) \\
       \cup (communication \cup payments) \cup (providers \cup users)
$$

### Actors 

All computational components are implemented both as actors and as graph vertices which allows to leverage both flexibility and asynchrony of the actor model of computation and indirect communication via evolving shared data structure enabled by graph computing. In NuNet platform ontology, Actors are `Nodes` and `Allocations`, which implement both Actor interface and Graph interface for implementing the logic of the computing model.

Proposed interfaces (currently discussed and updated for integration into the next release of the device-management-service):

* [Actor](https://gitlab.com/nunet/open-api/platform-data-model/-/blob/proposed/device-management-service/models/actor.go?ref_type=heads);
* [Node](https://gitlab.com/nunet/open-api/platform-data-model/-/blob/proposed/device-management-service/dms/node.go?ref_type=heads), implementing Actor;
* [Allocation](https://gitlab.com/nunet/open-api/platform-data-model/-/blob/proposed/device-management-service/jobs/allocation.go?ref_type=heads), implementing Actor
* [Graph](https://gitlab.com/nunet/open-api/platform-data-model/-/blob/proposed/device-management-service/models/graph.go?ref_type=heads) -- a placeholder for the graph traversal logic;

### Executables

***Executables*** or programs are well defined processes (AI agents, programs, container images...), which, when executed, take specific $data$ as input and produce some $data$ as output (which should be at least well formed). Note, that programs will be executed on a concrete hardware (machines) provided to the NuNet network by independent compute providers, which is the main focus of NuNet decentralized deployment and orchestration mechanism.

Major related `device-management-service` packages:

* [`executor`](https://docs.nunet.io/public-documentation/public-technical-documentation/device-management-service/develop/executor), abstracting executable interface from the containerization frameworks and executors themselves; current implementation contains two sub-packages:
    * [docker](https://docs.nunet.io/public-documentation/public-technical-documentation/device-management-service/develop/executor/docker) for running docker containers;
    * [firecracker](https://docs.nunet.io/public-documentation/public-technical-documentation/device-management-service/develop/executor/firecracker) for running firecracker virtual machines;
* [`orchestrator`](https://gitlab.com/nunet/device-management-service/-/tree/develop/orchestrator), implementing search and match compute resources with posted job requirements and sending the job to fitting hardware resource provider in the network;
* [`jobs`](https://gitlab.com/nunet/device-management-service/-/tree/develop/jobs) takes care of the job management on each Node (machine);


### Data

***Data*** as input and output data structures as defined above; note, that $data$ can actually be a program, which can be executed and then it becomes an $executable$, therefore the boundary is not definable a-priori, but is resolved dynamically -- which is important to do for our model;

Current proposals related `device-management-service` packages / functionalities:

* [`storage`](https://docs.nunet.io/public-documentation/public-technical-documentation/device-management-service/develop/storage) package;

_note by @kabir.kbr 2025-05-15: updated with references to current interface proposals until here_

### State

***State*** is the compute state at every moment of execution; in some sense this is also can be considered $data$, when, say, a computation is stopped, a snapshot is saved and then restarted at an other place. However, the way we will be treating this two kinds of data is different, therefore it makes sense to distinguish them in the model;

When reasoning about state of decentralized computing platform / engine, it is useful to separate three distinct aspects:

* ***Private state*** of an Actor which is essentially a state of the algorithmic implementation of business logic / program that an actor is running and is closest to the conventional understanding of program / execution state. While actor model allows for Actors to maintain state (obviously), it however constraints other parts of the actor system to access that state -- therefore it is private for the running Actor itself and cannot be shared outside its boundaries. In order to enforce this principle, NuNet is keeping clear Actor implementation boundaries in form of interfaces and their implementations. NuNet, however, considers the notion of 'persistent execution state' in its design and implementation, which in this context is a mechanism that allows to preserve snapshots of the private Actor space and move them across different compute environments - which does not contradict the constraints of the model.

* ***Global state*** refers to the notion of a execution state of the whole platform. We do not have this notion at NuNet and do not use it in design or implementation considerations. The notion of global state of any reasonably complex system (also computing engine and NuNet platform as a whole) is quite problematic on at least epistemological grounds, since one needs to posit a global omniscient observer in order to reason in terms of this concept. For architecture design of NuNet platform it is suffice to mention that positing global state and global observer contradicts Actor model and is therefore not used.

* ***Job orchestration state*** (for the lack of a better word in the context of NuNet platform architecture) refers to the state of a subset of compute processes within the platform, which directly interact with each other and may need to coordinate their actions in order to achieve the goal of a complex job and have some knowledge of each other's state. In order to comply to the Actor model, this shall be done only via message passing mechanism and strictly respecting the privacy of Actor states.

Proposed interfaces related to the notions (although no currently proposed interfaces / implementation) is directly related to state management at NuNet:

* [Job_interface](https://gitlab.com/nunet/open-api/platform-data-model/-/blob/proposed/device-management-service/jobs/job.go?ref_type=heads)
* [Allocation_interface](https://gitlab.com/nunet/open-api/platform-data-model/-/blob/proposed/device-management-service/jobs/allocation.go?ref_type=heads)

### Resources

***Resources*** are simply hardware resources (memory, processing power, GPU/CPU devices, sensors, actuators...) that are needed for executables to process the data; note, that a special case of 'processing data' is a a sensor which actually collects data from environment and executes effects on the larger environment. In NuNet design, we also use the notion of `Capability`, which also encompass hardware resources, in order to consider other properties of the execution environment (like libraries, containerization system, etc.).

Related [currently proposed] interfaces and types:

* [Capability](https://gitlab.com/nunet/open-api/platform-data-model/-/blob/proposed/device-management-service/dms/capability.go?ref_type=heads)
* [Resource](https://gitlab.com/nunet/open-api/platform-data-model/-/blob/proposed/device-management-service/dms/data/resource.payload.go?ref_type=heads)

### Connections

***Connections*** are a special resources within the computing system, which is may be defined as resources that is needed for data to travel from one executable to another; these resources have their own special properties (speed, bandwidth, reliability, price...) and, while in centralized computing they are usually taken for granted, in for reasoning about decentralized computing, they necessarily have to be elevated to the first class citizens of computational ecosystem.

In order to realize the notion of computational reflection, which is required to implement 'compute over data' aspect of a decentralized computing system, each Actor should be able to access data and reason about the properties of links and connections between itself and other hardware and software actors of the system. 

Related proposed interfaces / types;

* [Graph_interface](https://gitlab.com/nunet/open-api/platform-data-model/-/blob/proposed/device-management-service/models/graph.go?ref_type=heads);
* [JobLink](https://gitlab.com/nunet/open-api/platform-data-model/-/blob/proposed/device-management-service/jobs/jobLink.go?ref_type=heads);

### Communications

***Communication*** are the actual processes that are using $connections$. In our model of computation, all communications are defined as a set of well defined messages, therefore to some extent $communications$ can be considered equivalent to $messaging$

Related proposed interfaces:

* [Message](https://gitlab.com/nunet/open-api/platform-data-model/-/blob/proposed/device-management-service/telemetry/message.go)
* [Mailbox](https://gitlab.com/nunet/open-api/platform-data-model/-/blob/proposed/device-management-service/models/mailbox.go)

### Payments

***Payments*** is the necessary component of an economics system, or, in our case, a tokenomics system; the design and logic of the tokenomics system itself is discussed in detail in a separate section (see **D. Tokenomics**), which is as important as the model of computation; for defining the economy of computation at the present level of abstraction it is enough to say that the system should be able to provide a layer that will enable every actor in the system to engage into economic/tokenomic transactions;

### Providers and users

***Providers*** are the soco-economic entities (legal or natural persons) that are owners of the components of the computational universe. Since we are designing the economy of computing which is grounded in the social and economic fabric of society as a whole, we necessarily have to include this component into the model; Besides that, the system has to provide interfaces that are necessary for providers to manage their resources.

***Users*** are are also socio-economic entities which are using the computational universe to achieve their goals in the larger socio-economic environment. The main category of users are application developers which will run their application backends on NuNet;

Related resources:
* [NuNet Framework Stakeholder Mapping](https://nunet.gitlab.io/research/blog/posts/nunet-framework/) - high level stakeholder mapping concepts published as a research blog post
* [Low lever stakeholder mapping](https://gitlab.com/nunet/architecture/-/tree/develop/mapping?ref_type=heads) maintained as gitlab repo and shall be updated constantly;

## Architecture of open-ended computing

This section summarizes the concept of the NuNet computing model. It is best to read it after referring to subsections on **B.1 Computing model/Actor model** and **B.2 Computing model/Graph computing**.

The architecture for open-ended decentralized computing unites the actor model and graph computing into one framework:

* The actor model enables massively scalable implementation of a population of independent and heterogeneous computational processes, and their custom behaviours. Actors can have wildly different computational complexities and resource requirements. Computational processes of actors communicate with each other by passing mes- sages which encapsulate commands and data structures. The totality of all traces of messages constitute a dynamic message-passing graph – a shared data structure – where every actor is also a vertex. 
* Graph computing enables actors to access and change this graph by executing vertex-centric graph traversals based on their unique perspectives and custom behaviours. The message-passing graph, while best reasoned about in terms of a logical structure, is actually scattered across local memories of actors and does not constitute a single object. Therefore, it is not accessible directly, but only via the fact that an actor which accesses it is itself a part of the graph and can issue traversals from its location and via its links with immediate neighbours. Most importantly, the notion of message and graph traversal are different aspects of the same communication event. This fact alone unites the actor model and graph computing into one framework by allowing actors to specify legal paths of messages. In this way messages (i.e. information and control flow) propagate not randomly or directly, but depending on internal logic and the local graph structures, which develop and change because of other messages. 
* Actor model allows the operation of the framework to be radically decentralized and asynchronous, while graph computing allows for local computations. The framework enables the bottom-up emergence of computation graphs to be realized as persistent assemblages (sub-graphs) of computational processes and, in general, stigmergic computing and progressive determination. This is a high-level description of the architecture of open-ended decentralized computing in a nutshell. Figure below depicts the architecture by showing the actor system and graph computing engine  as integrated aspects. 

<img src="https://gitlab.com/nunet/publisher/platform-yellow-paper/-/raw/main/b_computing-model/assets/open-ended-computing-actor_system.png" alt="Open ended decentralized computing: Actor system" width=100%>

<img src="https://gitlab.com/nunet/publisher/platform-yellow-paper/-/raw/main/b_computing-model/assets/open-ended-computing-graph_computing.png" alt="Open ended decentralized computing: Graph computing" width=100%>
