# Platform development model

It may appear that platform development model does not belong in the `technical-yellow-paper` context, but the technology and especially current status if its development considering the overall vision is related to the process. It will be explained here.

*TBD*

## Platform components

See [nunet/architecture/mapping/](https://gitlab.com/nunet/architecture/-/tree/develop/mapping?ref_type=heads).

## Use-case integrations

## Platform integrations

## Licencing structure

See [NuNet Licencing Policy](https://docs.nunet.io/nunet-licensing-policy).