# Platform Yellow-Paper

This document is aimed to outline, describe and specify (and work iteratively on) the general NuNet model of computing. It expands on the NuNet [initial white-paper](https://docs.nunet.io/nunet-whitepaper/?utm_campaign=Whitepaper+website) and and conceptual ideas outlined there taking into account implementation work done so far as well as additional research done [or in progress] at NuNet as well as broader SingularityNET ecosystem so far.

The immediate goal is to design [as much as possible] semi-formal specification of the computing model that NuNet is implementing for guiding the technical work done by the core development team (see [open development board](https://gitlab.com/groups/nunet/-/boards/4356023)) as well as broader developer community (join our [discord server](https://discord.com/invite/8S7JNRBHJa) to participate). 

Besides, above and around the immediate goal of design specification, we attempt to describe and specify as much as possible the conceptual basis for specifications in the light of the overall vision of NuNet as a [global economy of decentralized computing](https://nunet.io).

Finally, this document / yellow-paper attempts to show relations between all somewhat loosely technically connected protocol specifications, design patterns, workflow constructions, API specifications, languages, research areas, etc. More often than not, these areas can be related only via reference to the conceptual vision of the computational ecosystem that NuNet aims to help bootstrapping.

This yellow-paper is intended to be part of living public technical documentation of NuNet. While we do not expect main aspects of the content of this document to change or be updated too frequently, it is still subject to change and update along the path of development. The paper is broken in seven parts, some of them more developed than others. These are (in the order of importance):

* **A. Ecosystem** introduces a basis of the general business model, as much as relevant for the technical architecture;
* **B. Computing model** explains conceptual architecture of the platform with relations to current implementations and technical decisions;
* **C. Observability Framework** enables to monitor and analyze computational events;
* **D. Tokenomics** is the interoperability layer between computational engine and crypto-micro-payments layer, including the micro-payments itself;
* **E. Platform Development Model** explains use-case based platform development model and provides details related to the general business model;
* **F. Necessary architectural aspects** are technical aspects that are needed for the overall NuNet's technical vision to be achieved;
* **G. Advanced architectural aspects** may not be necessary for achieving the technical vision but are important for larger context within SingularityNET ecosystem.

_maintained by @kabir.kbr; please assign all merge requests for edit of this page accordingly._
