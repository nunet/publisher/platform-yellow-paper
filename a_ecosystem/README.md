# Ecosystem of computations

We distinguish the conceptual/dynamic and technical aspects of the ecosystem into:

* **Framework**
* **Platform**

## Framework: global economy of decentralized computing

As a framework, NuNet will enable to position AI processes, interfaces and data optimally within the global network and establish machine-to-machine payment and data streaming channels between diverse constituents. We do not know what will be the precise constituents of the ecosystem in order to keep it open and encourage its evolution, but we can fairly well position them into the *constituent classes*. NuNet's stakeholder mapping is maintained by:

* NuNet Framework Stakeholder Mapping [research blog](https://nunet.gitlab.io/research/blog/posts/nunet-framework/);
* Stakeholder mapping [sub-project](https://gitlab.com/nunet/architecture/-/tree/develop/mapping), aimed at constantly updated categories and their description.

## Platform: computing model

Main conceptual components of NuNet platform are **Actor model** and its relation to network topology via **Graph computing** concepts. Taken together and related via lower level implementation packages, they constitute **NuNet computing model**, largely inspired by prior research of [open-ended computing](https://kabirkbr.github.io/public/synthetic_cognitive_development/kabir_thesis_20190321_official_submission.pdf#chapter.5). Another two major components of the platform are **Observability framework** (a.k.a. Telemetry) and **Tokenomics**. The latter is most directly related to the implementation of decentralized computing economy model, inspired and framed by [offer networks research](https://kabirkbr.github.io/public/synthetic_cognitive_development/kabir_thesis_20190321_official_submission.pdf#chapter.6). 

Interaction of these four major aspects constitute the technological basis for global economy of decentralized computations. 

## NuNet Platform + Framework: Economy of computations

Technically, NuNet platform is a set of open APIs, communication protocols – and open source software components which expose those APIs and protocols – that enables the global economy of the decentralized computing: the framework that connects compute providers, data providers, AI developers, application developers into a single cloud (see picture above).

For the sake of engineering the economy of interacting computational processes, we define the global network of computations (without much ambition for theoretical generality) as follows:

$$
     NuNet = \\
       (executables \cup data \cup state) \\ 
       \cup (resources \cup connections) \\
       \cup (communication \cup payments) \\
       \cup (providers \cup users)
$$

As mentioned above, the mapping between NuNet framework's stakeholders and platform's components is maintained in [nunet/architecture/mapping](https://gitlab.com/nunet/architecture/-/tree/develop/mapping) repository and includes:

1. [stakeholder categories](https://gitlab.com/nunet/architecture/-/tree/develop/mapping/stakeholder_categories.md);
1. [stakeholders](https://gitlab.com/nunet/architecture/-/tree/develop/mapping/stakeholders.md);
1. [component classes](https://gitlab.com/nunet/architecture/-/blob/develop/mapping/component_classes.md);
1. [components](https://gitlab.com/nunet/architecture/-/tree/develop/mapping/components.md)

Further detalization of these categories and their relations to specific platform packages is provided in the **Computing model** section.