# Tokenomic/economic model

In order to support a system in which actors would autonomously yet collectively search for assemblages leading to "better" configurations, as required by different tasks and problems, we need the notion and implementation of ***computational economy**. Such an economy would define the costs of physical computing resources and data transfer that actors would need to take into account when forming assemblages – or larger computational units. Moreover, in order to describe such an economy, notions of actors’ location, mobility and execution context should be defined in a way that could be accessed and reasoned by the actors themselves. This requires an extension of the otherwise location-transparent actor model with a global yet dynamic topological structure that can be accessed and modified by actors.

We implement the basis / fundamentals of tokenomic model by designing and implemented the abstract interoperability layer that allows computing model (and deployment layer) to interact with any micro-payments infrastructure that is required for business needs / use-cases of different user classes participating in the network. 

## Related implementations

* The basic abstract interface and its implementation is developed within [device-management-service/tokenomics](https://gitlab.com/nunet/device-management-service/-/tree/develop/tokenomics?ref_type=heads) package;

## External tokenomics

*TBD*

## Network tokenomics

*TBD*